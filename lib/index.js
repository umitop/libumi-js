'use strict';

var HelloWorld = (function () {
    function HelloWorld() {
    }
    HelloWorld.prototype.helloWorld = function () {
        return 'Hello World!';
    };
    return HelloWorld;
}());

exports.HelloWorld = HelloWorld;
