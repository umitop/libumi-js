var HelloWorld = (function () {
    function HelloWorld() {
    }
    HelloWorld.prototype.helloWorld = function () {
        return 'Hello World!';
    };
    return HelloWorld;
}());

export { HelloWorld };
