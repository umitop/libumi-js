const HelloWorld = require('../dist').HelloWorld
const assert = require('chai').assert

describe('index', function () {
  it('helloWorld', function () {
    const expected = 'Hello World!'
    const actual = new HelloWorld().helloWorld()

    assert.strictEqual(actual, expected)
  })
})
